# PyNmap

A simple script that leverages python3 to execute big nmap scans multi-threaded. This script allows you to scan massive networks without relying on boring slow old nmap.

## Usage

A few variables need to be set before scanning. Each are placed at the top of the python file as shown below:

```python
port = 4075
threads = 50
scanlistfile = 'toscanlist'
```

### `ports` variable

Either a single port or comma separated list of ports.

### `threads` variable

The maximum amount of asynchronous scanning instances of nmap.

### `scanlistfile` variable

Input file of targets to scan. Can be in any nmap parsable format.
