import nmap
import concurrent.futures

ports = 4075
threads = 50
scanlistfile = 'toscanlist'

def doscan(target):
    res = []
    nm = nmap.PortScanner()
    nm.scan(target, ports, arguments='-Pn -vvv -n --host-timeout 5')
    for res in nm.all_hosts():
        for port in ports.split(','):
            try:
                if nm[res]['tcp'][int(port)]['state'] == 'open':   # Magic spaghetti that gets all targets with 'port' open
                    with open('results.txt','w') as f:
                        f.write(str(res) + '\n')
            except:
                print('[!] EXCEPTION RAISED IN {}:{} THREAD'.format(target,port))
                pass #Needed to avoid crashing over hosts without any tcp results
    print('[*] Last completed scan: {}'.format(target))
    
with open(scanlistfile,'r') as f:
    scanlist = f.readlines()

print(f"[*] Starting scan with {str(scanlist.__len__())} targets and {str(threads)} workers...")
    
with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as executor:
    executor.map(doscan, scanlist)
    
print("[!] We're done here, see results.txt for results")
